import setuptools

REQUIRED_PACKAGES = ['dnspython==1.16.0', 'pymongo==3.9.0', 'pytz==2019.1']

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mongo-logger",
    version="0.3.4",
    author="Sharif Elfouly",
    author_email="selfouly@gmail.com",
    description="Write your logs in your mongo database.",
    long_description=long_description,
    packages=setuptools.find_packages(),
    install_requires=REQUIRED_PACKAGES,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
