import uuid
import time
import pymongo

from datetime import datetime, timedelta
from pymongo import MongoClient
from pytz import timezone

# All the timestamps will be from this timezone. This is important, because
# otherwise the timestamps would be different.
BERLIN_TIMEZONE = timezone('Europe/Berlin')


class MongoLogger:
    def __init__(self, mongo_uri, database_name, topic, print_to_console=False):
        """
        You can use the Mongo Logger to easily write logs to your mongo
        database.

        When you initialize the MongoLogger object you will have to
        define a topic to which these logs belong. A new table in the database
        (defined by `database_name`) with the name of the topic will be created.
        There all your logs will be stored.

        Args:
            mongo_uri (str): The uri of the mongo database. You can read about
                it here:
                https://docs.mongodb.com/manual/reference/connection-string/.
            database_name (str): Name of the database where the logs will be
                saved.
            topic (str): Topic to which these logs belong.
            print_to_console (bool): Should the logs be printed out to the
                console. Defaults to `False`.
        """
        self.topic = topic
        self.print_to_console = print_to_console

        self.logging_active = True

        self.client = MongoClient(mongo_uri)

        self.logs_collection = self._init_db(self.client, database_name)

    def _init_db(self, client, db_name):
        """
        Initialize the database and return the logs collection.
        Args:
            client (pymongo.Client): The mongo client.
            db_name (str): The unique name of the database.
        Returns:
            logs_collection (pymongo.collection.Collection): Every log will be
                saved here.
        """
        db_mongo = client[db_name]

        logs_collection = db_mongo[self.topic]

        return logs_collection

    def _log(self, log):
        """
        Writes a log to the database in the topics table.
        Args:
            log (Log)
        """
        if not self.logging_active:
            return

        if self.print_to_console:
            print(log)

        self._write_to_db(log)

    @staticmethod
    def _is_valid_tag(tag):
        """
        Checks if the tag is valid. If not it raises an Exception.
        """
        if tag is None:
            return

        valid_tags = ['DEBUG', 'INFO', 'WARNING', 'ERROR']

        if tag not in valid_tags:
            raise Exception(
                'Your tag can only be one of these 4 choices: DEBUG/INFO/WARNING/ERROR'
            )

    def stop_logging(self):
        """Stop the logging."""
        self.logging_active = False

    def continue_logging(self):
        """Continue logging"""
        self.logging_active = True

    def debug(self,
              message,
              inspection_id=None,
              point_id=None,
              image_name=None,
              frame_number=None):
        """
        Write a debug log to the database.
        Args:
            message (str): The message you want to log.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (str): Optional.
            frame_number (int): Optional
        """
        log = Log('DEBUG', message, self.topic, inspection_id, point_id,
                  image_name, frame_number)

        self._log(log)

    def info(self,
             message,
             inspection_id=None,
             point_id=None,
             image_name=None,
             frame_number=None):
        """
        Write a info log to the database.
        Args:
            message (str): The message you want to log.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (str): Optional.
            frame_number (int): Optional
        """
        log = Log('INFO', message, self.topic, inspection_id, point_id,
                  image_name, frame_number)

        self._log(log)

    def warning(self,
                message,
                inspection_id=None,
                point_id=None,
                image_name=None,
                frame_number=None):
        """
        Write a warning log to the database.
        Args:
            message (str): The message you want to log.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (str): Optional.
            frame_number (int): Optional.
        """
        log = Log('WARNING', message, self.topic, inspection_id, point_id,
                  image_name, frame_number)

        self._log(log)

    def error(self,
              message,
              inspection_id=None,
              point_id=None,
              image_name=None,
              frame_number=None):
        """
        Write an error log to the database.
        Args:
            message (str): The message you want to log.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (str): Optional.
            frame_number (int): Optional.
        """
        log = Log('ERROR', message, self.topic, inspection_id, point_id,
                  image_name, frame_number)

        self._log(log)

    def _write_to_db(self, log):
        """
        Write this logs to the logs table in the database.
        Args:
            log (Log)
        """
        log_document = {
            'id': str(uuid.uuid1()),
            'timeStamp': log.time_stamp,
            'tag': log.tag,
            'message': log.message
        }

        if log.topic is not None:
            log_document['topic'] = log.topic
        if log.inspection_id is not None:
            log_document['inspectionId'] = log.inspection_id
        if log.point_id is not None:
            log_document['pointId'] = log.point_id
        if log.image_name is not None:
            log_document['imageName'] = log.image_name
        if log.frame_number is not None:
            log_document['frameNumber'] = log.frame_number

        self.logs_collection.insert_one(log_document)

    def _get_last_log(self):
        """
        Returns the last log of this topic.
        """
        c = self.logs_collection.find_one({'topic': self.topic},
                                          sort=[('timeStamp',
                                                 pymongo.DESCENDING)])

        last_log = Log.from_dict(c)

        return last_log

    def get_logs(self):
        cursor = self.logs_collection.find({'topic': self.topic})

        logs = []
        for c in cursor:
            log = Log.from_dict(c)

            logs.append(log)

        return logs

    def get(self,
            n,
            start_date=None,
            end_date=None,
            tag=None,
            inspection_id=None,
            point_id=None,
            image_name=None,
            frame_number=None):
        """
        Get the last n logs of your specific topic and tag between a specific
        time.
        Args:
            n (int): Number of logs to return.
            start_date (datetime.datetime): Returns every log after this date.
            end_date (datetime.datetime): Returns every log before this date.
            tag (None): Optional. If None all logs of all tags will be returned.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (int): Optional.
            frame_number (str): Optional.
        """
        MongoLogger._is_valid_tag(tag)

        query = {
            'topic': self.topic,
        }

        if tag is not None:
            query['tag'] = tag

        if start_date is not None:
            start_date_timestamp = datetime.timestamp(start_date)
            query['timeStamp'] = {'$gt': start_date_timestamp}

        if end_date is not None:
            end_date_timestamp = datetime.timestamp(end_date)

            try:
                query['timeStamp']['$lt'] = end_date_timestamp
            except KeyError:
                query['timeStamp'] = {'$lt': end_date_timestamp}

        if inspection_id is not None:
            query['inspectionId'] = inspection_id

        if point_id is not None:
            query['pointId'] = point_id

        if image_name is not None:
            query['imageName'] = image_name

        if frame_number is not None:
            query['frameNumber'] = frame_number

        cursor = self.logs_collection.find(query,
                                           sort=[('timeStamp',
                                                  pymongo.DESCENDING)
                                                 ]).limit(n)

        logs = []
        for c in cursor:
            log = Log.from_dict(c)

            logs.append(log)

        return logs

    def get_live(self):
        """
        Continuously print out the last log of this specific topic.
        WARNING: This method doesn't end.
        """
        time_last_log = 0.0

        while True:
            new_log = self._get_last_log()

            if time_last_log != new_log.time_stamp:
                time_last_log = new_log.time_stamp

                print(new_log)

    def close(self):
        """Close the mongo connection"""
        self.client.close()

    def __del__(self):
        """Close the mongo connection when the MongoLogger object is destroyed"""
        self.close()


class Log:
    def __init__(self,
                 tag,
                 message,
                 topic,
                 inspection_id=None,
                 point_id=None,
                 image_name=None,
                 frame_number=None,
                 time_stamp=None):
        """
        Args:
            tag (str): One of four choices: (DEBUG, INFO, WARNING, ERROR)
            message (str): The log message.
            topic (str): Defaults to None. Use this to give your log a specific
                topic.
            inspection_id (str): Optional.
            point_id (str): Optional.
            image_name (str): Optional.
            frame_number (str): Optional
            time_stamp (float): time in seconds. The output of time.time().
        """
        tags = ['DEBUG', 'INFO', 'WARNING', 'ERROR']
        if tag not in tags:
            raise Exception(
                'Your tag can only be one of these 4 choices: DEBUG/INFO/WARNING/ERROR'
            )

        if time_stamp is None:
            self.time_stamp = datetime.now(BERLIN_TIMEZONE).timestamp()
        else:
            self.time_stamp = time_stamp

        self.tag = tag
        self.message = message
        self.topic = topic
        self.inspection_id = inspection_id
        self.point_id = point_id
        self.image_name = image_name
        self.frame_number = frame_number

    def __str__(self):
        """
        Creates the log string.
        """
        date_time = Log.get_datetime(self.time_stamp)

        log_string = f'{date_time} - {self.tag} - {self.message}'

        if self.inspection_id is not None:
            log_string += f' - {self.inspection_id}'

        if self.point_id is not None:
            log_string += f' - {self.point_id}'

        if self.image_name is not None:
            log_string += f' - {self.image_name}'

        if self.frame_number is not None:
            log_string += f' - {self.frame_number}'

        return log_string

    @staticmethod
    def from_dict(log_dict):
        """
        Create a log from a dict.
        Args:
            log_dict (dict): Attributes of the log as a dictionary.
        """
        try:
            topic = log_dict['topic']
        except KeyError:
            topic = None

        try:
            inspection_id = log_dict['inspectionId']
        except KeyError:
            inspection_id = None

        try:
            point_id = log_dict['pointId']
        except KeyError:
            point_id = None

        try:
            image_name = log_dict['imageName']
        except KeyError:
            image_name = None

        try:
            frame_number = log_dict['frameNumber']
        except KeyError:
            frame_number = None

        last_log = Log(log_dict['tag'], log_dict['message'], topic,
                       inspection_id, point_id, image_name, frame_number,
                       log_dict['timeStamp'])

        return last_log

    @staticmethod
    def get_datetime(seconds):
        """
        Creates a time strings.
        Args:
            seconds (float): This is time.time().
        Returns:
            dt (datetime.datetime): The time as a datetime object.
        """
        struct = time.gmtime(seconds)

        dt = datetime.fromtimestamp(time.mktime(struct))

        # To get the time in the Berlin Timezone.
        dt += timedelta(hours=1)

        return dt
