
# mongo-logger

Write your logs easily in your mongo database.

## VERY IMPORTANT
**Do not change the public methods or the initialization of the mongo logger. 
You can only change them if the parameters are optional.**

If they are changed all applications that use the mongo logger in production **WILL** break.

Those methods are:
 - debug
 - info
 - warning
 - error

## Install

This package was only tested with **python 3**. Using python 2 could result in
unexpected behaviour.

Install this package very easily via pip:
`pip3 install git+https://bitbucket.org/vi_datascience/mongo-logger/src/master/`

Don't forget the + after git.

## How to use

#### Write Logs
If you have ever used the build in logging module, this package works very similar. You have 4 methods to write logs. Those are:

  - debug
  - info
  - warning
  - error

#### View Logs

There are two methods you can use. Those are:
- get()
- get_live()

With the `get()` method you can return a number of logs that
can be sorted by the following parameters:

  - n
  - start_date (Optional)
  - end_date (Optional)
  - tag (Optional)
  - inspection_id (Optional)
  - point_id (Optional)
  - image_name (Optional)
  - frame_number (Optional)

The `get_live()` method returns every log in real time. Use this
if you want to look at running systems in real time.

### Close the connection

You can close the connection of the MongoLogger database, with its `close()` method.

## Example

Look at mongo_logger/example.py for an example on how to use the Mongo Logger.

## How to update the pip package
1) Make your changes in the code as always.

2) Upgrade the version in the setup.py file.

3) Run the following command in your terminal:
`python3 setup.py bdist_wheel`
